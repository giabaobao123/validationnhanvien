function NhanVien(taikhoan, ten, email, pass, date, luongCB, valueChucVu, sogiolam) {
  this.taikhoan = taikhoan;
  this.ten = ten;
  this.email = email;
  this.pass = pass;
  this.date = date;
  this.luongCB = luongCB;
  this.valueChucVu = valueChucVu;
  this.sogiolam = sogiolam;
  this.chucVu = function(){
    var tenChucVu = ""
    if (this.valueChucVu==3) {
       tenChucVu = "Sếp"
    }
    else if (this.valueChucVu==2) {
       tenChucVu = "Trưởng phòng"
    }
    else if (this.valueChucVu==1) {
        tenChucVu = "Nhân viên"
    }
    return tenChucVu
 }
 this.XepLoai = function () {
   if (this.sogiolam>=192) {
       return "Xuất Sắc"        
   }
   else if (this.sogiolam>=176) {
       return "Giỏi"
   }
   else if (this.sogiolam>=160){
       return "Khá"
   } 
   else {
       return "Trung bình"
   }
 }
 this.TongLuong = function () { 
    return this.luongCB * this.valueChucVu 
  }

}
function DomID(id) {
  return document.getElementById(id)
}
